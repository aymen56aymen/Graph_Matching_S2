#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "declaration.h"
/*


void tricroissant( int a[], int b )
{
    int i=0;
    int x=0;
    int j=0;
 
    for(i=0;i<b;i++)
    {
        for(j=1;j<b;j++)
        {
            if(a[i]<a[j])
            {
                x=a[i];
                a[i]=a[j];
                a[j]=x;
                j--;
                }
 
        }
 
        }
 
    x=a[0];
    for(i=0;i<b;i++)
    a[i]=a[i+1];
    a[b-1]=x;
 
}
//-------------------------------------------------------------
int loc_somm( int SIZE , int size , int somm){
int a ;
a = somm/((SIZE/size)+1);
if(somm <= (((SIZE/size)+1)*(SIZE%size))) if (0 == somm%((SIZE/size)+1)) a--;
 


return(a);
}

//-------------------------------------------------------------------
Liste1 *initialisation1(){

    Liste1 *liste1 = malloc(sizeof(*liste1));

    Element1 *element1 = malloc(sizeof(*element1));


    if (liste1 == NULL || element1 == NULL) {

        exit(EXIT_FAILURE);

    }


    element1->nombre = 0;

    element1->suivant1 = NULL;

    liste1->premier1 = element1;


    return liste1;

}


//-------------------------------------------------------------------
void insertion1(Liste1 *liste1, int nvNombre)

{

   

    Element1 *nouveau1 = malloc(sizeof(*nouveau1));

    if (liste1 == NULL || nouveau1 == NULL)

    {

        exit(EXIT_FAILURE);

    }

    nouveau1->nombre = nvNombre;


    nouveau1->suivant1 = liste1->premier1;

    liste1->premier1 = nouveau1;

}
//-----------------------------------------------------------

void afficherListe1(Liste1 *liste)

{

    if (liste == NULL)

    {

        exit(EXIT_FAILURE);

    }


    Element1 *actuel = liste->premier1;


    while (actuel != NULL)

    {

        printf("%d -> ", actuel->nombre);

        actuel = actuel->suivant1;

    }

    printf("NULL\n");

}
//************************ FONCTION ****************


sommet* mes_vois(int* tab, int sendcouts , int SIZE , int comm , int rank){
Liste1* l = initialisation1(); 

int sz; 


for(int i = 0 ; i < SIZE-sendcouts ; i++){
for(int j = 0 ; j < SIZE ; j++){

if (tab[i*SIZE+j] == 1){

 Element1 *temp = l->premier1; 

 
        while(temp->suivant1 != NULL){ 
             if ((temp->nombre) == j+1 ){

                 break;
                 }
            temp = temp->suivant1;
          
        }
        if(temp->suivant1 == NULL) insertion1(l,j+1);


      
                                     }
}}

 Element1 *temp = l->premier1; 

    sz = -1;
    while(temp != NULL){sz++; temp = temp->suivant1;}

int ta[sz];
int ps;
ps = 0 ;
    Element1 *actuel = l->premier1;

    while (actuel != NULL){ ta[ps] = (actuel->nombre); actuel = actuel->suivant1; ps++; }
   tricroissant( ta, sz );
   int nb_p ; 
   nb_p = 1;
    int xv;
    for (xv = 0 ; xv < sz;xv++) if(loc_somm( SIZE , comm , ta[xv]) != x){nb_p++; x = loc_somm( SIZE , comm , ta[xv]);}

int*    mdisp = malloc(nb_p * sizeof(int));
sommet*    msendcounts = malloc(nb_p * sizeof(sommet));
for ( int cv = 0 ; cv < nb_p ; cv++){
mdisp[cv] = 0;
msendcounts[cv].deg = 0;
}

int av;
av = 0;
for (xv = nb_p-1 ; xv >= 0;xv--){
for (xw = 0 ; xw < sz;xw++) if(loc_somm( SIZE , comm , ta[xw]) == x) {msendcounts[xv].deg++;msendcounts[xv].somm = x;}
x = loc_somm( SIZE , comm , ta[xv]);














 
//------------------------------
summm = 0;
for (xv = 0 ; xv < nb_p;xv++){
mdisp[xv] = summm; 
summm+= msendcounts[xv].deg;
}


}
 }*/

//************************ FONCTION ****************
void out_put(int* resul_matching , int SIZE){

    FILE* fichier = NULL;
    fichier = fopen("RESULTAT", "w");
    {

        fprintf(fichier, "graph GraphMatching{\n");
        for ( int i = 0 ; i< SIZE ; i++) {
        fprintf(fichier,"%d [shape = circle, color = blue];\n", i+1);
                                      }

         for(int i = 0; i < SIZE ; i++)
	  {
		for (int j = i+1; j < SIZE; j++)
		{
			if ( resul_matching[i*SIZE+j] == 1 ){
                          fprintf(fichier,"%d--%d;\n", i+1,j+1);
		                                         }

                       else if (((resul_matching[i*SIZE+j] == 3 ) && ( resul_matching[j*SIZE+i] != 3 )) || ((resul_matching[i*SIZE+j] != 3 ) && ( resul_matching[j*SIZE+i] == 3 ))){
				fprintf(fichier,"%d--%d;\n", i+1,j+1);
				                                                              }


			else if ((resul_matching[i*SIZE+j] == 3 ) &&( resul_matching[j*SIZE+i] == 3 )){
				fprintf(fichier,"%d--%d[color=red,penwidth=3.0];\n", i+1,j+1);
				                                                              }
		}
	}

        fprintf(fichier,"}");
        fclose(fichier);
    }
 }


//************************ FONCTION ****************
Graph lire_data(){

Graph G;
int a;

 
FILE* fichier = NULL; 
fichier = fopen("G.text", "r");

fscanf(fichier, "%d", &a);

G.data = malloc((a*a)*sizeof(int));

 
        for (int w = 0 ; w < a ; w++) {
        for (int v = 0 ; v < a ; v++) {
      fscanf(fichier, "%d", &(G.data[w*a+v])) ;
                                               }
}
    
G.sommet = a;    
fclose(fichier);     
    return(G);
} 

//************************ FONCTION ****************

void completer_couplage(int* tab , int SIZE){

int i , c, r;
i = 0;
c = 0;
r = 0;
while(i<SIZE){
          for(int j = 0 ; j < SIZE; j++){
              if (tab[j*SIZE+i] != 3) c++;
                                        }
          if( c == SIZE ){
                    for(int j = 0 ; j < SIZE; j++){
                    if ((tab[j*SIZE+i] == 1) && (tab[j*SIZE+i] == 1)){
                             for(int a = 0 ; a < SIZE ; a++){
                              if (tab[a*SIZE+j] != 3) r++;
                                                            }
                    if( r == SIZE ){
                      tab[j*SIZE+i] = 3;
                      tab[i*SIZE+j] = 3;
                                   }
                        r = 0 ;
                                                             }
                                                   }
           c = 0 ;
                         }
i++;


             }
}

//************************ FONCTION ****************

sommet voisin_choisit(sommet* tab , int l ) {

sommet a ; 

a.somm = tab[0].somm;
a.deg =tab[0].deg;
 
for ( int i = 0 ; i < l ; i++){
if (tab[i].deg < a.deg){ 
a.deg = tab[i].deg; 
a.somm = tab[i].somm ; 
}

}
return(a);
}

//************************ FONCTION ****************



sommet* get_mes_vois( sommet* tab_somm,int l_tab_somm,int inf, int* tabi , int l , int moi ){
sommet* mes_vois ;
int j = 0;
int ec = 0;
while ((j <= l_tab_somm) && ( tab_somm[j].somm != moi )) j++ ;

mes_vois = malloc((tab_somm[j].deg)*sizeof(sommet));


  for(int i = inf; i < l; ++i){  
   if (tabi[i] == 1 ) {
    mes_vois[ec].somm = -(i-inf+1);
    mes_vois[ec].deg = tab_somm[i-inf].deg;
         ec++;
                      }
                             
}
return(mes_vois);

}

          
//********************* FONCTION *********************


sommet* caract_somm(int* src, int l, int pas){
  
int sum;
sommet* dst = (sommet*)malloc((l/pas)*sizeof(sommet));

  for(int i = 0; i < l/pas; ++i){     
    for(int j= 0; j < pas; ++j){

                               if (src[i*pas+j] == 1 ){
                               sum++;
                                                      }
                                else if (src[i*pas+j] < 0){
                                dst[i].somm = src[i*pas+j];
                                                           }
      
                               }
       dst[i].deg = sum;
       sum = 0 ;
                                 }
  return dst;
                                              }






//                         _____________________________________
//                        |                                     |
//                        |                                     |
//********************    |                MAIN                 |   *********************
//                        |                                     |
//                        |                                     |
//                         _____________________________________








int main(int argc, char *argv[])
{
    int* tabi;
    sommet *tab_sommet;
    int rank, size;     
    int *sendcounts;
    int *sendcounts2;    
    int *disp; 
    int *disp2;      
    
    
    int sum = 0;
    int sum2 = 0;
    Graph G; 
    int SIZE;
      
    int* resul_matching;
    int* data;

 

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

//--------------------------------------------

    MPI_Aint intex;
    int count = 2;
    int blocks[2] = {1,1};
    MPI_Datatype type[2] = {MPI_INT, MPI_INT};
    MPI_Datatype MPI_SOMMET;
    MPI_Aint displacements[2];
    MPI_Type_extent (MPI_INT, &intex);
    displacements[0]=(MPI_Aint)(0);
    displacements[1] = intex;

    MPI_Type_struct(count,blocks,displacements,type, &MPI_SOMMET);
    MPI_Type_commit(&MPI_SOMMET);

    
//--------------------------------------------


    sendcounts = malloc(sizeof(int)*size);
    sendcounts2 = malloc(sizeof(int)*size);
    disp = malloc(sizeof(int)*size);
    disp2 = malloc(sizeof(int)*size);

if ( 0 == rank){
    G = lire_data();       
    SIZE = G.sommet;
   resul_matching = malloc(SIZE*SIZE*(sizeof(int)));


               }
MPI_Bcast(&SIZE, 1, MPI_INT, 0, MPI_COMM_WORLD);



int test = SIZE % size;

for(int i = 0; i < size; i++)
{

  if(i < test)
  {
    sendcounts[i] = SIZE*((SIZE / size) + 1);
    sendcounts2[i] = (SIZE/size) + 1;
  }
  else
  {
    sendcounts[i] = SIZE*(SIZE / size) ;
    sendcounts2[i] = (SIZE/size) ; 
  }
  disp[i] = sum;
  disp2[i] = sum2;
  sum += sendcounts[i];
  sum2 += sendcounts2[i];
  
}

if(rank < test)
{
  tabi = malloc(SIZE*((SIZE / size) + 1)*sizeof(int));

}
else
{
  tabi = malloc(SIZE*(SIZE / size )*sizeof(int));
  
}


tab_sommet = malloc( SIZE * sizeof(sommet));





                                    
                    

    // ------------------------------------------------------------------------------------------------------------------------
   
    MPI_Scatterv(G.data, sendcounts, disp, MPI_INT, tabi,sendcounts[rank], MPI_INT, 0, MPI_COMM_WORLD);

   //--------------------------CALCUL SOMMET ------------------


    
   sommet* tab_sommeti = caract_somm(tabi, sendcounts[rank],SIZE);
   

//------------------------AFFICHE------------------------

 /* if ( 1 == rank ){
   for (int i = 0; i < sendcounts[rank]; i++) {
    printf(" (%d) ", tabi[i]);
    
                                               }printf("\n");
                    }


   if ( 1 == rank ){
   for (int i = 0; i < size_tab_sommeti; i++) {
    printf(" (V%d , %d)", tab_sommeti[i].somm, tab_sommeti[i].deg);
    printf("\n");
                                               }
                    }*/

//-------------------------------------
 
   MPI_Gatherv(tab_sommeti,sendcounts2[rank], MPI_SOMMET, tab_sommet, sendcounts2, disp2, MPI_SOMMET, 0, MPI_COMM_WORLD);


//------------------------AFFICHE------------------------
/*
if ( 0 == rank ) {
for ( int i = 0 ; i < SIZE ; i++ ) {
printf("(V%d --%d)",tab_sommet[i].somm,tab_sommet[i].deg);
} printf("\n");
              
                 
}

*/
//-----------------------------------------------

  MPI_Bcast(tab_sommet, SIZE, MPI_SOMMET, 0, MPI_COMM_WORLD);


 
//---------------------------------------------***********************************************************



int pas = sendcounts[rank]/sendcounts2[rank];
int ci = 0;

for(int a = 0 ; a < sendcounts2[rank] ; a++){

sommet moi = tab_sommeti[a];
sommet* mes_vois;
mes_vois = get_mes_vois(tab_sommet,SIZE,ci,tabi,ci+pas, -1);



//-----------------
/*
printf("\n");
for ( int i = 0 ; i < moi.deg; i++ ) {
printf("S(%d)--> (%d)  |",mes_vois[i].somm,mes_vois[i].deg);
}printf("\n"); 
*/
//-----------------


sommet s;
s = voisin_choisit( mes_vois, moi.deg );


//-----------------
/*
printf("\n"); 
printf("vois choisit :  S(%d)--> (%d)",s.somm,s.deg);
printf("\n");
*/
//-----------------

 
tabi[a*pas-(s.somm+1)] = 3;

ci+=pas;


//-----------------
/*
printf("\n");
for ( int i = 0 ; i <moi.deg-1; i++ ) {
printf("(%d) ",tt[i]);
} 
printf("\n"); 
printf("\n");
for ( int i = 0 ; i < sendcounts[rank]; i++ ) {
printf("(%d) ",tabi[i]);
} 
printf("\n"); 
*/
//-----------------


               

}           
                 

//----------------------------------------------------
   MPI_Gatherv(tabi,sendcounts[rank], MPI_INT, resul_matching, sendcounts, disp, MPI_INT, 0, MPI_COMM_WORLD);


//--------------------------------

 if ( 0 == rank ) {
completer_couplage(resul_matching,SIZE);
printf("\n");
 for (int i = 0; i < SIZE; i++) {
 for (int j = 0; j < SIZE; j++) {
    printf(" (%d)", resul_matching[i*SIZE+j]);
    
                                 }printf("\n");
                  }



}




//------------------------------------
   if ( 0 == rank ) out_put(resul_matching , SIZE) ;
    




    MPI_Finalize();
    free(sendcounts);
    free(sendcounts2);
    free(tab_sommet);
    free(tab_sommeti);
    free(disp);
    free(disp2);

    return 0;
}





